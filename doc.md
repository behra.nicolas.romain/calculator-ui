# Calculator Ui

Frontend app to visualize and use the calculator-server API
Using Angular 5 as requested.

# Initialization
Node version: v14.17.0
npm version: 6.14.13

```sh
> npm install -g @angular/cli@1.6.6
> ng new calculator-ui
```

# Serve de application

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
