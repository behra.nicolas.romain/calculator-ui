import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '@environments/environment';
import { CalculatorModel } from '@models/calculator.model';
import { ECalculatorType } from '@models/enums';

@Injectable()
export class CalculatorService {

  private static TOKEN = 'tokenTest123';

  private static HEADERS = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
    'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
    'Authorization': `${ CalculatorService.TOKEN }`,
  };

  constructor(private httpClient: HttpClient) { }

  async getCardsByShopId(shopId: number, amount: number): Promise<Map<ECalculatorType, CalculatorModel>> {
    const params = {
      'amount': amount + ''
    };

    return this.httpClient.get(
      environment.apiServerUrl + `shop/${ shopId }/search-combination`,
      {
        headers: CalculatorService.HEADERS,
        params: params
      }
    )
    .toPromise()
    .then(
      (data: any) => {

        return CalculatorModel.asCalculatorModels(data);
      }
    );

  }

}
