import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { option } from 'ts-option';

import { CalculatorModel } from '@models/calculator.model';
import {
  EAmountType,
  ECalculatorType
} from '@models/enums';

import { CalculatorService } from './calculator.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent {

  amountForm : FormGroup;
  amountCtrl : AbstractControl;
  isValidating: boolean = false;
  hasResponse: boolean = false;
  calculators: Map<ECalculatorType, CalculatorModel>;
  amountType: EAmountType = EAmountType.NONE;
  nextAmountTmp: number = 0;
  previousAmountTmp: number = Infinity;
  error: string = '';

  @Output() amount = new EventEmitter<number>();

  private static SHOP_ID = 5;

  constructor(
    public form: FormBuilder,
    private calculatorService: CalculatorService
  ) {
    this.calculators = new Map<ECalculatorType, CalculatorModel>();
    this.initForm();
  }

  get EAmountType() {
    return EAmountType;
  }

  next(event: Event) {
    this.error = '';

    if (
      option(this.amountCtrl.value).isDefined
      && this.amountCtrl.valid
    )
      this.nextAmountTmp = +this.amountCtrl.value;
    else
      this.nextAmountTmp = 0;

    this.nextAmountTmp++;

    return this.getCards(CalculatorComponent.SHOP_ID, this.nextAmountTmp)
    .then(
      (calculators: Map<ECalculatorType, CalculatorModel>) => {
        const amountType: EAmountType = CalculatorModel.getEAmountType(calculators);

        if (amountType === EAmountType.EQUAL) {
          this.amountType = CalculatorModel.getEAmountType(calculators);
          this.amountCtrl.setValue(this.nextAmountTmp);

          return this.calculators = calculators;
        }

        if (amountType === EAmountType.LOWER_AND_HIGHER)
          return this.validate(null, calculators.get(ECalculatorType.CEIL).value);

        if (amountType === EAmountType.HIGHER)
          return this.validate(null, calculators.get(ECalculatorType.CEIL).value);

        this.error = 'Pas de valeur suivante possible';

        return new Map<ECalculatorType, CalculatorModel>();
      }
    );
  }

  previous(event: Event) {

    if (
      option(this.amountCtrl.value).isDefined
      && this.amountCtrl.valid
    )
      this.previousAmountTmp = +this.amountCtrl.value;
    else
      this.previousAmountTmp = Infinity;

    this.previousAmountTmp--;

    return this.getCards(CalculatorComponent.SHOP_ID, this.previousAmountTmp)
    .then(
      (calculators: Map<ECalculatorType, CalculatorModel>) => {
        const amountType: EAmountType = CalculatorModel.getEAmountType(calculators);

        if (amountType === EAmountType.EQUAL) {
          this.amountType = CalculatorModel.getEAmountType(calculators);
          this.amountCtrl.setValue(this.previousAmountTmp);

          return this.calculators = calculators;
        }

        if (amountType === EAmountType.LOWER_AND_HIGHER)
          return this.validate(null, calculators.get(ECalculatorType.FLOOR).value);

        if (amountType === EAmountType.LOWER)
          return this.validate(null, calculators.get(ECalculatorType.FLOOR).value);

        this.error = 'Pas de valeur précédente possible';

        return new Map<ECalculatorType, CalculatorModel>();
      }
    );
  }

  async validate(event: Event, amount?: number): Promise<Map<ECalculatorType, CalculatorModel>> {
    if (event)
      event.stopPropagation();

    if (option(amount).isDefined && amount !== this.amountCtrl.value)
      this.amountCtrl.setValue(amount);

    this.amount.emit(this.amountCtrl.value);

    this.isValidating = true;
    await this.getCardsProcess();
    this.isValidating = false;

    return this.calculators;
  }

  private async getCards(
    shopId: number,
    amount: number
  ): Promise<Map<ECalculatorType, CalculatorModel>> {
    return this.calculatorService.getCardsByShopId(
      shopId,
      amount
    );
  }

  private async getCardsProcess(): Promise<Map<ECalculatorType, CalculatorModel>> {
    this.error = '';
    return this.getCards(
      CalculatorComponent.SHOP_ID,
      this.amountCtrl.value
    )
    .then(
      (calculators: Map<ECalculatorType, CalculatorModel>) => {
        this.amountType = CalculatorModel.getEAmountType(calculators);
        this.calculators = calculators;

        return this.calculators;
      }
    )
    .catch(err => {
      this.amountType = EAmountType.NONE;
      this.calculators = new Map<ECalculatorType, CalculatorModel>();
      console.error('Error: ', err);
      this.error = 'Une erreur est survenue';

      return this.calculators;
    });
  }

  private initForm(): void {

    this.amountForm = this.form.group(
      this.addInputs());

    this.relyInputControls();
  }

  private addInputs(): any {
    return {
      amountCtrl : [ '', [
        Validators.min(0),
        Validators.required
      ] ]
    };
  }

  private relyInputControls(): void {
    this.amountCtrl = this.amountForm.controls['amountCtrl'];
  }
}
