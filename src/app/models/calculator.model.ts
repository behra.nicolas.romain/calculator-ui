import { EAmountType, ECalculatorType } from "./enums";


export class CalculatorModel {

  cards: Array<number>;
  value: number;

  constructor()

  constructor(
    _cards: Array<number>,
    _value: number
  )

  constructor(
    _cards?: Array<number>,
    _value?: number
  ) {
    this.cards = _cards || [];
    this.value = _value || 0;
  }

  static getEType(type: string): ECalculatorType {
    if (type.toLowerCase() === 'floor')
      return ECalculatorType.FLOOR;

    if (type.toLowerCase() === 'equal')
      return ECalculatorType.EQUAL;

    return ECalculatorType.CEIL;
  }

  static asCalculatorModel(json: any): CalculatorModel {
    const cards: Array<number> = json['cards'];
    const value: number        = json['value'];

    const calculator = new CalculatorModel(
      cards,
      value
    );

    return calculator;
  }

  static asCalculatorModels(json: any): Map<ECalculatorType, CalculatorModel> {
    const map: Map<ECalculatorType, CalculatorModel> = new Map<ECalculatorType, CalculatorModel>();

    if (!json)
      return map;


    Object.keys(json).forEach(
      (key: string) => {

        map.set(
          CalculatorModel.getEType(key),
          CalculatorModel.asCalculatorModel(json[key])
        )
      }
    )

    return map;
  }

  static getEAmountType(calculators: Map<ECalculatorType, CalculatorModel>): EAmountType {
    if (calculators.has(ECalculatorType.EQUAL))
      return EAmountType.EQUAL;

    if (
      calculators.has(ECalculatorType.FLOOR)
      && calculators.has(ECalculatorType.CEIL)
    )
      return EAmountType.LOWER_AND_HIGHER;

    if (calculators.has(ECalculatorType.FLOOR))
      return EAmountType.LOWER;

    if (calculators.has(ECalculatorType.CEIL))
      return EAmountType.HIGHER;

    return EAmountType.NONE;
  }

}
