
export enum ECalculatorType {
  FLOOR = -1,
  EQUAL = 0,
  CEIL = 1
}

export enum EAmountType {
  NONE = -2,
  LOWER = -1,
  EQUAL = 0,
  HIGHER = 1,
  LOWER_AND_HIGHER = 2
}
